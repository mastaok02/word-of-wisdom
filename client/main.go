package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net"
	"strings"
)

const (
	serverAddress = "localhost:8080"
	difficulty    = 4 // Must match the server's difficulty
)

func main() {
	conn, err := net.Dial("tcp", serverAddress)
	if err != nil {
		fmt.Println("Error connecting to server:", err)
		return
	}
	defer conn.Close()

	reader := bufio.NewReader(conn)
	seed, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Error reading seed:", err)
		return
	}
	seed = strings.TrimSpace(seed)

	nonce := solvePoW(seed, difficulty)
	_, err = conn.Write([]byte(nonce + "\n"))
	if err != nil {
		fmt.Println("Error sending nonce:", err)
		return
	}

	quote, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Error reading quote:", err)
		return
	}
	fmt.Println("Received quote:", quote)
}

func solvePoW(seed string, difficulty int) string {
	var nonce int
	for {
		hash := sha256.Sum256([]byte(seed + fmt.Sprint(nonce)))
		hexHash := hex.EncodeToString(hash[:])
		if strings.HasPrefix(hexHash, strings.Repeat("0", difficulty)) {
			return fmt.Sprint(nonce)
		}
		nonce++
	}
}
