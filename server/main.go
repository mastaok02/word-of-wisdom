package main

import (
	"bufio"
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	mRand "math/rand"
	"net"
	"strconv"
	"strings"
)

const (
	port       = ":8080"
	difficulty = 4 // Number of leading zeros required in the hash
)

var quotes = []string{
	"The only true wisdom is in knowing you know nothing.",
	"The unexamined life is not worth living.",
	"To find yourself, think for yourself.",
	"Wisdom begins in wonder.",
	"Be kind, for everyone you meet is fighting a hard battle.",
}

func main() {
	listener, err := net.Listen("tcp", port)
	if err != nil {
		fmt.Println("Error starting server:", err)
		return
	}
	defer listener.Close()
	fmt.Println("Server is listening on port", port)

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting connection:", err)
			continue
		}
		go handleConnection(conn)
	}
}

func handleConnection(conn net.Conn) {
	defer conn.Close()

	reader := bufio.NewReader(conn)
	seed := generateSeed()
	_, err := conn.Write([]byte(seed + "\n"))
	if err != nil {
		fmt.Println("Error sending seed:", err)
		return
	}

	nonce, err := reader.ReadString('\n')
	if err != nil {
		fmt.Println("Error reading nonce:", err)
		return
	}
	nonce = strings.TrimSpace(nonce)

	if !validatePoW(seed, nonce, difficulty) {
		fmt.Println("Invalid PoW solution")
		return
	}

	quote := quotes[mRand.Intn(len(quotes))]
	_, err = conn.Write([]byte(quote + "\n"))
	if err != nil {
		fmt.Println("Error sending quote:", err)
	}
}

func generateSeed() string {
	var b [8]byte
	_, err := rand.Read(b[:])
	if err != nil {
		// Handle error
		fmt.Println("Error generating seed:", err)
		return ""
	}
	return strconv.FormatUint(binary.BigEndian.Uint64(b[:]), 10)
}

func validatePoW(seed, nonce string, difficulty int) bool {
	hash := sha256.Sum256([]byte(seed + nonce))
	hexHash := hex.EncodeToString(hash[:])
	return strings.HasPrefix(hexHash, strings.Repeat("0", difficulty))
}
